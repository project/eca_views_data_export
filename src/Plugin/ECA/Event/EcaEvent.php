<?php

namespace Drupal\eca_views_data_export\Plugin\ECA\Event;

use Drupal\Core\Form\FormStateInterface;
use Drupal\eca\Attributes\Token;
use Drupal\eca\Entity\Objects\EcaEvent as BaseEcaEvent;
use Drupal\eca\Plugin\DataType\DataTransferObject;
use Drupal\eca\Plugin\ECA\Event\EventBase;
use Drupal\eca_views_data_export\EcaEvents;
use Drupal\eca_views_data_export\Event\AlterRow;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Plugin implementation of the ECA Events for eca_views_data_export.
 *
 * @EcaEvent(
 *   id = "eca_views_data_export",
 *   deriver = "Drupal\eca_views_data_export\Plugin\ECA\Event\EcaEventDeriver",
 *   eca_version_introduced = "1.0.0"
 * )
 */
class EcaEvent extends EventBase {

  /**
   * {@inheritdoc}
   */
  public static function definitions(): array {
    $definitions = [];
    $definitions['alter_row'] = [
      'label' => 'Alter a row',
      'event_name' => EcaEvents::ALTER_ROW,
      'event_class' => AlterRow::class,
    ];
    return $definitions;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    if ($this->eventClass() === AlterRow::class) {
      $form['view_id'] = [
        '#type' => 'textfield',
        '#title' => $this->t('View ID'),
        '#default_value' => $this->configuration['view_id'] ?? '',
      ];
      $form['display_id'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Display ID'),
        '#default_value' => $this->configuration['display_id'] ?? '',
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function generateWildcard(string $eca_config_id, BaseEcaEvent $ecaEvent): string {
    if ($this->getDerivativeId() === 'alter_row') {
      $configuration = $ecaEvent->getConfiguration();
      return (isset($configuration['view_id']) ? trim($configuration['view_id']) : '') . '::' . (isset($configuration['display_id']) ? trim($configuration['display_id']) : '');
    }
    return parent::generateWildcard($eca_config_id, $ecaEvent);
  }

  /**
   * {@inheritdoc}
   */
  public static function appliesForWildcard(Event $event, string $event_name, string $wildcard): bool {
    [$view_id, $display_id] = explode('::', $wildcard);
    return ($event instanceof AlterRow) && ($view_id === '' || $event->getView()->id() === $view_id) && ($display_id === '' || $event->getView()->current_display === $display_id);
  }

  /**
   * {@inheritdoc}
   */
  #[Token(
    name: 'current_result',
    description: 'The result object of the event.',
  )]
  #[Token(
    name: 'current_row',
    description: 'The row object of the event.',
  )]
  public function getData(string $key): mixed {
    /** @var \Drupal\eca_views_data_export\Event\AlterRow $event */
    $event = $this->getEvent();
    return match ($key) {
      'current_result' => DataTransferObject::create($event->getResult()),
      'current_row' => DataTransferObject::create($event->getRow()),
      default => parent::getData($key),
    };
  }

}
