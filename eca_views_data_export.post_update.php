<?php

/**
 * @file
 * Post update functions for ECA Views Data Export.
 */

/**
 * Rename used tokens in available ECA models for ECA 2.0.0.
 */
function eca_views_data_export_post_update_rename_tokens_2_0_0(): void {
  $tokenNames = [
    'current-result' => 'current_result',
    'current-row' => 'current_row',
  ];
  _eca_post_update_token_rename($tokenNames);
}
