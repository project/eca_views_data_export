<?php

/**
 * @file
 * Tokens provided by ECA Views Data Export module.
 */

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function eca_views_data_export_token_info(): array {
  $info = [];
  $info['types']['current_result'] = [
    'name' => t('Current views result'),
    'needs-data' => 'current_result',
    'nested' => TRUE,
    'dynamic' => TRUE,
  ];
  $info['types']['current_row'] = [
    'name' => t('Current views row'),
    'needs-data' => 'current_row',
    'nested' => TRUE,
    'dynamic' => TRUE,
  ];
  $info['tokens']['current_result']['dummy'] = [
    'name' => t('Just a dummy'),
  ];
  $info['tokens']['current_row']['dummy'] = [
    'name' => t('Just a dummy'),
  ];
  return $info;
}

/**
 * Implements hook_tokens().
 */
function eca_views_data_export_tokens(string $type, array $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata): array {
  if ($type === 'current_result' && !empty($data['current_result'])) {
    $data['dto'] = $data['current_result'];
    unset($data['current_result']);
    return eca_tokens('dto', $tokens, $data, $options, $bubbleable_metadata);
  }
  if ($type === 'current_row' && !empty($data['current_row'])) {
    $data['dto'] = $data['current_row'];
    unset($data['current_row']);
    return eca_tokens('dto', $tokens, $data, $options, $bubbleable_metadata);
  }
  return [];
}
